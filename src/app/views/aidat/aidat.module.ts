import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AidatComponent } from './aidat.component';

import { AidatRoutingModule } from './aidat-routing.module';

@NgModule({
  declarations: [AidatComponent],
  imports: [
    CommonModule,
    AidatRoutingModule
  ]
})
export class AidatModule { }
