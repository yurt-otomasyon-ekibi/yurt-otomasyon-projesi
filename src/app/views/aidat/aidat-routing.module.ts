import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { AidatComponent } from './aidat.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Aidat'
    },
    children: [
        {
            path: '',
            redirectTo: 'aidat'
          },
          {
            path: 'aidat',
            component: AidatComponent,
            data: {
              title: 'Aidat'
            }
          }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AidatRoutingModule {}