import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OgrenciComponent } from './ogrenci.component';

import { OgrenciRoutingModule } from './ogrenci-routing.module';

@NgModule({
  declarations: [OgrenciComponent],
  imports: [
    CommonModule,
    OgrenciRoutingModule
  ]
})
export class OgrenciModule { }
