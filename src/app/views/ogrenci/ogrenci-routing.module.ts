import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { OgrenciComponent } from './ogrenci.component';

const routes: Routes = [
  {
    path: '',
    component: OgrenciComponent,
    data: {
      title: 'Öğrenci'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OgrenciRoutingModule {}