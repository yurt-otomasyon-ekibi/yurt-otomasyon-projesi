import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { PersonelComponent } from './personel.component';

const routes: Routes = [
  {
    path: '',
    component: PersonelComponent,
    data: {
      title: 'Personel'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonelRoutingModule {}