import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonelComponent } from './personel.component';

import { PersonelRoutingModule } from './personel-routing.module';

@NgModule({
  declarations: [PersonelComponent],
  imports: [
    CommonModule,
    PersonelRoutingModule
  ]
})
export class PersonelModule { }
