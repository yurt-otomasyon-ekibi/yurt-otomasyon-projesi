import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DaireListeComponent } from './daire-liste/daire-liste.component';

import { DaireRoutingModule } from './daire-routing.module';

@NgModule({
  declarations: [DaireListeComponent],
  imports: [
    CommonModule,
    DaireRoutingModule
  ]
})
export class DaireModule { }
