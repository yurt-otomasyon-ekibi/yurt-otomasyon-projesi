import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { DaireListeComponent } from './daire-liste/daire-liste.component';

const routes: Routes = [
  {
    path: '',
    component: DaireListeComponent,
    data: {
      title: 'Daire'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DaireRoutingModule {}