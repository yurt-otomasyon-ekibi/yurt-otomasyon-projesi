import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BinaEkleComponent } from './bina-ekle/bina-ekle.component';

import { BinaRoutingModule } from './bina-routing.module';

@NgModule({
  declarations: [BinaEkleComponent],
  imports: [
    CommonModule,
    BinaRoutingModule
  ]
})
export class BinaModule { }
