import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BinaEkleComponent } from './bina-ekle.component';

describe('BinaEkleComponent', () => {
  let component: BinaEkleComponent;
  let fixture: ComponentFixture<BinaEkleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BinaEkleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BinaEkleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
