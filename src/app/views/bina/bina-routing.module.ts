import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { BinaEkleComponent } from './bina-ekle/bina-ekle.component';

const routes: Routes = [
  {
    path: '',
    component: BinaEkleComponent,
    data: {
      title: 'Bina'
    }
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BinaRoutingModule {}