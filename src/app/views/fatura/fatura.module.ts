import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaturaComponent } from './fatura.component';

import { FaturaRoutingModule } from './fatura-routing.module';

@NgModule({
  declarations: [FaturaComponent],
  imports: [
    CommonModule,
    FaturaRoutingModule
  ]
})
export class FaturaModule { }
