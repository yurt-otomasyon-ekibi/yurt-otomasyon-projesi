import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';

import { FaturaComponent } from './fatura.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Fatura'
    },
    children: [
        {
            path: '',
            redirectTo: 'fatura'
          },
          {
            path: 'fatura',
            component: FaturaComponent,
            data: {
              title: 'Fatura'
            }
          }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FaturaRoutingModule {}